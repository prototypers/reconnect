const connect = require('../connect');
const sendMessage = require('./webex');

let previousState;
let listener = 0;

const device = {
  host: process.env.HOST,
  username: process.env.USER,
  password: process.env.PASSWORD || '',
};

const botToken = process.env.TOKEN;
const email = process.env.EMAIL;

if (!device.host || !device.username) {
  console.log('Usage: you need to specify HOST and USERNAME (and PASSWORD) for device');
  process.exit(1);
}

async function onStateChanged(state, xapi) {
  console.log(new Date(), 'xapi connection:', state);

  if (xapi) {
    listener++;
    const id = listener;
    xapi.Status.Audio.Volume.on((vol) => {
      console.log(new Date(), 'Listener', id, 'volume', vol);
      xapi.Command.UserInterface.Message.Alert.Display({ Text: `Volume: ${vol}%`, Duration: 1 });
    });
  }

  if (botToken && email) {
    // dont want to be notified on every unsuccesful reconnect attempt
    if (state === 'connected' || (state === 'disconnected' && previousState === 'connected')) {
      const message = `Webex device **${device.host}** changed state to **${state}**`
      sendMessage(botToken, email, message);
    }
  }

  previousState = state;
}

connect(device, onStateChanged);
