/** simple lib to send a webex message. no error handling */
const https = require('https');

const url = 'https://webexapis.com/v1/messages';

function sendMessage(token, email, message) {
  const postBody = { toPersonEmail: email, markdown: message };

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
    }
  };

  const req = https.request(url, options);
  req.write(JSON.stringify(postBody));
  req.end();
}

module.exports = sendMessage;
