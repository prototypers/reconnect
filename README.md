# Persistent jsxapi connection

The purpose of this micro library is to provide a persistent jsxapi connection
to a Webex Device, that automatically re-connects if the connection goes down, whether
it is due to software upgrade on the device, device being rebooted, network issues etc.

Features:

* Auto-reconnect
* Periodic heart-beat to detect that the connection is actually live
* Exponential back-off on retries
* De-registers all previous listeners when reconnecting
* Notify client about connection state


Example usage: An external script that records the number of people in a meeting room over time.

```
const connect = require('./connect');

const device = { host: '10.47.1.2', username: 'admin', password: '1234' };

connect(device, onConnectionChanged);

// This function will be called any time the connection state changes:
function onConnectionChanged(state, xapi) {

  // when connected:
  if (state === 'connected') {
    console.log('connected');
    xapi.Status.RoomAnalytics.PeopleCount.Current.on((value) => {
      console.log('People count:' + value);
    });
  }

  // device will be in this state when connection goes down, before trying to reconnect:
  else if (state === 'disconnected') {
    console.log('disconnected');
  }

  // when trying to reconnect again:
  else if (state === 'connecting') {
    console.log('connecting to device');
  }
}
```
