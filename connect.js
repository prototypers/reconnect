/**
 * Todo:
 * - make configs available to user
 */
const jsxapi = require('jsxapi');

// How often to send heart beat
const HeartBeatFreqSec = 10;

// The longest time to wait before reconnecting
const MaxReconnectWaitSec = 10 * 60;

// How long to wait before heart beat times out
const HeartBeatTimeoutSec = 2;

const State = {
  connecting: 'connecting',
  connected: 'connected',
  disconnected: 'disconnected',
};

let lastDevice;
let onStateChange;
let heartbeatTimer;
let reconnectTimer;
let state = '';
let reconnectWait = 1; // in seconds, use for exponential backoff
let xapi;

function connect(device, callback) {
  onStateChange = callback;
  lastDevice = device;

  notifyState(State.connecting);

  const { host, username, password } = device;
  console.log('connecting to', device);
  jsxapi
    .connect('wss://' + host, {
      username: username,
      password: password,
    })
    .on('error', error => {
      if (state === State.disconnected) {
        return; // usually comes twice per incident
      }
      notifyState(State.disconnected);
      reconnect();
    })
    .on('ready', (_xapi) => {
      xapi = _xapi;
      onConnected(_xapi);
    });
}

function wait(time) {
  return new Promise(resolve => setTimeout(() => resolve('timeout'), time * 1000))
}

function notifyState(newState, xapi) {
  if (newState === state) return;
  state = newState;
  onStateChange(state, xapi);
}

async function reconnect() {
  if (xapi) {
    console.log('close xapi');
    await xapi.removeAllListeners();
    xapi.close();
  }

  reconnectWait = Math.min(reconnectWait * 2, MaxReconnectWaitSec);
  console.log('reconnect in', reconnectWait);
  clearInterval(heartbeatTimer);

  reconnectTimer = setTimeout(() => {
    connect(lastDevice, onStateChange);
  }, reconnectWait * 1000);
}

async function heartBeat(xapi) {
  try {
    const value = await Promise.race([
      xapi.Status.SystemUnit.Uptime.get(),
      wait(HeartBeatTimeoutSec),
    ]);
    if (value === 'timeout') {
      console.warn(new Date(), 'Lost heartbeat');
      notifyState(State.disconnected);
      reconnect();
    }
    // console.log('Ping volume:' + value);
  }
  catch(e) {
    reconnect();
    console.log('Ping failed', e);
  }
}

async function onConnected(xapi) {
  clearTimeout(reconnectTimer);
  clearInterval(heartbeatTimer);
  onStateChange(State.connected, xapi);
  reconnectWait = 1; // reset
  heartbeatTimer = setInterval(() => heartBeat(xapi), HeartBeatFreqSec * 1000);
}

module.exports = connect;
